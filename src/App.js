import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const people = ['Raphi', 'Raphi', 'Andi', 'David', 'Lukas', 'Sebi'];
const randomName = people[Math.floor(Math.random() * people.length)];

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
            Hoi {randomName}, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
